const MAX_NUM: u32 = 10000000;

fn next_item(mut n: u32) -> u32 {
    let mut sum: u32 = 0;
    while n != 0 {
        let digit = n % 10;
        sum += digit * digit;
        n /= 10;
    }
    return sum;
}

fn main() {
    let mut count = 0;
    for mut n in 1..MAX_NUM {
        while n != 1 && n != 89 {
            n = next_item(n);
        }
        if n == 89 {
            count += 1
        }
    }
    println!("{}", count);
}
