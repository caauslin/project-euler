fn main() {
    // factorials for number 0 to 9
    let mut factorial_table: [u32; 10] = [1; 10];
    for n in 1..10 {
        factorial_table[n] = n as u32 * factorial_table[n - 1];
    }

    // calculate upper ulimit
    let mut ulimit: u32 = 9;
    let mut ulimit_fsum: u32 = factorial_table[9];
    loop {
        if ulimit > ulimit_fsum {
            break;
        }
        ulimit = ulimit * 10 + 9;
        ulimit_fsum += factorial_table[9];
    }

    let mut result: u32 = 0;
    for n in 10u32..ulimit {
        // calculate digits factorial sum of n
        let mut n_tmp: u32 = n;
        let mut n_fsum: u32 = 0;
        loop {
            if n_tmp == 0 {
                break
            }
            n_fsum += factorial_table[(n_tmp % 10) as usize];
            n_tmp /= 10;
        }

        if n_fsum == n {
            result += n
        }
    }

    println!("{}", result);
}
