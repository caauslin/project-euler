fn main() {
    let mut sum = 0;
    for n in 1..1000 {
        sum += if n % 3 == 0 || n % 5 == 0 { n } else { 0 };
    }
    println!("{}", sum);
}
