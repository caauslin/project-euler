from euler import read_input


# TODO not very elegant
if __name__ == '__main__':
    numbers = [n for n in read_input(delim=(' ', '\n'), value_type=int)]
    max_p = 0
    for i in range(0, 20):
        for j in range(3, 20):
            hoz = (
                numbers[i * 20 + j] *
                numbers[i * 20 + j - 1] *
                numbers[i * 20 + j - 2] *
                numbers[i * 20 + j - 3]
            )
            vet = (
                numbers[j * 20 + i] *
                numbers[j * 20 + i - 20] *
                numbers[j * 20 + i - 40] *
                numbers[j * 20 + i - 60]
            )
            max_p = max(hoz, vet, max_p)
    for i in range(3, 20):
        for j in range(3, 20):
            dia_i = (
                numbers[i * 20 + j] *
                numbers[i * 20 + j - 21] *
                numbers[i * 20 + j - 42] *
                numbers[i * 20 + j - 63]
            )
            dia_r = (
                numbers[i * 20 + j - 3] *
                numbers[i * 20 + j - 22] *
                numbers[i * 20 + j - 41] *
                numbers[i * 20 + j - 60]
            )
            max_p = max(dia_i, dia_r, max_p)
    print(max_p)
