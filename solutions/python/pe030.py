import sys


def sum_of_digits_fifth_pow(n):
    """Sums the fifth powers of the digits of given number."""
    n_str, total = str(n), 0
    for d_char in n_str:
        total += pow(int(d_char), 5)
    return total


if __name__ == '__main__':
    # since 9^5 = 59,049(5) and 9^5 * 6 = 354,294(6), so this number should be
    # at most 6-digits long, more specifically, 354,294
    upper_limit, total = 354294, 0
    for n in range(2, upper_limit):
        if n == sum_of_digits_fifth_pow(n):
            total += n
    print(total)
