# FIXME performance deceases significantly when money increases
def count_coins_combinations(money, coins=(1, 2, 5, 10, 20, 50, 100, 200)):
    """Finds the total combinations of coins of given money.

    Args:
        money (int): The money value (in pence in this case).
        coins (Tuple<int>): A list of available coin values.

    Returns:
        int: The total combinations.
    """
    coins_len = len(coins)

    def count_with_limited_coin_value(money, max_coin_value=-1):
        total = 0
        for i in range(0, coins_len):
            if not ((max_coin_value == -1 or coins[i] < max_coin_value) and
                    money >= coins[i]):
                break
            if coins[i] == 1 or money == coins[i]:
                total += 1
            elif coins[i] == 2:
                total += money // 2
            else:
                for k in range(1, money // coins[i] + 1):
                    tmp = money - coins[i] * k
                    if tmp > 0:
                        total += count_with_limited_coin_value(tmp, coins[i])
                    elif tmp == 0:
                        total += 1
        return total
    return count_with_limited_coin_value(money)


if __name__ == '__main__':
    print(count_coins_combinations(200))
