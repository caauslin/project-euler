def next_item(n):
    result = 0
    while n != 0:
        d = n % 10
        result += d * d
        n //= 10
    return result


# takes ~40s
if __name__ == '__main__':
    count = 0
    for n in range(1, 10000000):
        while n != 1 and n != 89:
            n = next_item(n)
        if n == 89:
            count += 1
    print(count)

