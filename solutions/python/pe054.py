import collections
from euler import read_input_lines


PokerCard = collections.namedtuple('PokerCard', 'value suit')

CARD_VALUES = {
    '2': 2, '3': 3, '4': 4, '5': 5, '6': 6, '7': 7, '8': 8, '9': 9,
    'T': 10, 'J': 11, 'Q': 12, 'K': 13, 'A': 14
}

CARD_SUITS = {'C': 1, 'S': 2, 'H': 3, 'D': 4}


class PokerHand(object):

    def __init__(self, line):
        card_list = line.split(' ')
        if len(card_list) != 5:
            raise ValueError()
        cards = []
        for card in card_list:
            if len(card) != 2:
                raise ValueError()
            cards.append(PokerCard(CARD_VALUES[card[0]], CARD_SUITS[card[1]]))
        self.cards = sorted(cards, key=lambda card: card.value)
        self._process()

    def _process(self):
        is_same_suit = self.cards[0].suit == self.cards[1].suit and \
            self.cards[1].suit == self.cards[2].suit and \
            self.cards[2].suit == self.cards[3].suit and \
            self.cards[3].suit == self.cards[4].suit
        is_straight = self.cards[0].value + 1 == self.cards[1].value and \
            self.cards[1].value + 1 == self.cards[2].value and \
            self.cards[2].value + 1 == self.cards[3].value and \
            self.cards[3].value + 1 == self.cards[4].value

        if is_same_suit and is_straight:
            if self.cards[0].value == 10:
                self.rank = 1
                self.score = 0
            else:
                self.rank = 2
                self.score = self.cards[4].value
            return

        singles = []  # values for any single card
        pairs = []  # values for pairs
        triplet = None  # value for three of a kind
        quad = None  # value for four of a kind

        count = 1  # count for same-value card
        for i in range(0, len(self.cards)):
            if i < len(self.cards) - 1:
                if self.cards[i].value == self.cards[i + 1].value:
                    count += 1
                    continue
            if count == 1:
                singles.append(self.cards[i].value)
            elif count == 2:
                pairs.append(self.cards[i].value)
            elif count == 3:
                triplet = self.cards[i].value
            else:
                quad = self.cards[i].value
            count = 1

        if quad is not None:
            self.rank = 3
            self.score = quad * 16 + singles[0]
        elif triplet is not None and len(pairs) == 1:
            self.rank = 4
            self.score = triplet * 16 + pairs[0]
        elif is_same_suit:
            self.rank = 5
            self.score = 0
            for i in range(0, len(self.cards)):
                self.score += self.cards[i].value * 16 ** i
        elif is_straight:
            self.rank = 6
            self.score = self.cards[4].value
        elif triplet is not None:
            self.rank = 7
            self.score = singles[0] + singles[1] * 16 + triplet * 16 ** 2
        elif len(pairs) == 2:
            self.rank = 8
            self.score = singles[0] + pairs[0] * 16 + pairs[1] * 16 ** 2
        elif len(pairs) == 1:
            self.rank = 9
            self.score = singles[0] + singles[1] * 16 + \
                singles[2] * 16 ** 2 + pairs[0] * 16 ** 3
        else:
            self.rank = 10
            self.score = 0
            for i in range(0, len(self.cards)):
                self.score += self.cards[i].value * 16 ** i

    def __gt__(self, other):
        if self.rank < other.rank:
            return True
        elif self.rank == other.rank and self.score > other.score:
            return True
        else:
            return False


if __name__ == '__main__':
    result = 0
    for line in read_input_lines():
        p1_hand = PokerHand(line[:14])
        p2_hand = PokerHand(line[15:29])
        if p1_hand > p2_hand:
            result += 1
    print(result)
