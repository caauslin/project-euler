def is_lychrel(n, iteration=50):
    if iteration == 1:  # end of 50 iterations
        return True
    ra = n + int(str(n)[::-1])  # reverse and add
    ra_str = str(ra)
    if ra_str == ra_str[::-1]:  # check palindromic
        return False
    return is_lychrel(ra, iteration - 1)


if __name__ == '__main__':
    count = 0
    for n in range(1, 10000):
        if is_lychrel(n):
            count += 1
    print(count)
