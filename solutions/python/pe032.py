def pandigital_range(digits, num_length):
    """Returns a list of pandigital numbers formed by given digits with
    specific length.
    """
    def validate(n):
        """Checks for duplicate and digits."""
        n_str = str(n)
        for n_char in n_str:
            n_str = n_str.replace(n_char, '', 1)
            if (n_char in n_str) or (not n_char in digits):
                return False
        return True

    min_num = int(digits[:num_length])
    max_num = int(digits[:-num_length-1:-1])

    r = []
    for n in range(min_num, max_num + 1):
        if validate(n):
            r.append(n)

    return r


# TODO can be simplified
if __name__ == '__main__':
    total = 0
    numbers_str = '123456789'
    # given 'x * y = n', assume 'x', 'y' and 'n' is pandigital
    # loop through 'x' and 'y'
    # 'x_len' is numbers of digits in 'x', and 'y_len' for 'y'
    # set 'x_len >= y_len' to avoid duplicate combinations
    for y_len in range(1, 9):
        for x_len in range(y_len, 9):
            # the min digits in 'n': 'x_len + y_len - 1' and
            # the max digits in 'n': 'x_len + y_len', thus:
            if not (x_len + y_len + x_len + y_len - 1 == 9 or
                x_len + y_len + x_len + y_len == 9):
                continue
            # loop tho all the x
            for x in pandigital_range(numbers_str, x_len):
                # remove all the digits that used in 'x'
                numbers_str_y = numbers_str
                for c in str(x):
                    numbers_str_y = numbers_str_y.replace(c, '')

                min_y = int(numbers_str_y[:y_len])
                max_y = int(numbers_str_y[:-y_len-1:-1])

                if len(str(x * min_y)) + x_len + y_len > 9:
                    break
                elif len(str(x * max_y)) + x_len + y_len < 9:
                    continue

                for y in pandigital_range(numbers_str_y, y_len):
                    # calculate 'n'
                    n = x * y
                    # get total length
                    len_total = x_len + y_len + len(str(n))
                    # 'len_total' must equal to 9
                    if len_total > 9:
                        break
                    elif len_total < 9:
                        continue

                    left_numbers = numbers_str_y
                    duplicate = False
                    for c in str(n) + str(y):
                        if c in left_numbers:
                            left_numbers = left_numbers.replace(c, '')
                        else:
                            duplicate = True
                            break

                    if not duplicate:
                        # print('%d * %d = %d' % (x, y, n))
                        total += n
    print(total)
