from euler import is_prime


def is_bi_truncatable_prime(n):
    """Checks if a number is bi-directional truncatable prime."""
    if not is_prime(n):
        return False

    n_str = str(n)
    n_len = len(n_str)

    for i in range(n_len - 1):
        if not is_prime(int(n_str[:i+1])):
            return False
        if not is_prime(int(n_str[i+1:n_len])):
            return False
    return True


if __name__ == '__main__':
    count = 0
    n = 11
    total = 0
    while count < 11:
        if is_bi_truncatable_prime(n):
            count += 1
            total += n
        n += 2
    print(total)
