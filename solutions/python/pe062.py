from collections import defaultdict


def main():
    permutations = defaultdict(int)
    smallest_cubes = dict()
    n = 1
    while True:
        n_cube = n ** 3
        n_cube_key = str(sorted(str(n_cube)))
        if permutations[n_cube_key] == 4:
            return smallest_cubes[n_cube_key]
        elif permutations[n_cube_key] == 0:
            smallest_cubes[n_cube_key] = n_cube
        permutations[n_cube_key] += 1
        n += 1


if __name__ == '__main__':
    print(main())
