import math
from euler import is_prime


def is_positive_int(n):
    return n > 0 and math.ceil(n) == n


if __name__ == '__main__':
    n, primes, stop = 1, [2], False
    while not stop:
        n += 2

        if is_prime(n):
            primes.append(n)
            continue

        stop = True
        for prime in primes:
            square = (n - prime) / 2.0
            if not is_positive_int(square):
                continue
            square_root = math.sqrt(square)
            if not is_positive_int(square_root):
                continue
            stop = False
    print(n)
