import itertools


def has_divisibility_property(n_str):
    """Checks if the given number has the divisibility property mentioned in the
    problem.
    """
    div2 = int(n_str[1] + n_str[2] + n_str[3])
    if div2 % 2 != 0:
        return False
    div3 = int(n_str[2] + n_str[3] + n_str[4])
    if div3 % 3 != 0:
        return False
    div5 = int(n_str[3] + n_str[4] + n_str[5])
    if div5 % 5 != 0:
        return False
    div7 = int(n_str[4] + n_str[5] + n_str[6])
    if div7 % 7 != 0:
        return False
    div11 = int(n_str[5] + n_str[6] + n_str[7])
    if div11 % 11 != 0:
        return False
    div13 = int(n_str[6] + n_str[7] + n_str[8])
    if div13 % 13 != 0:
        return False
    div17 = int(n_str[7] + n_str[8] + n_str[9])
    if div17 % 17 != 0:
        return False
    return True


if __name__ == '__main__':
    total = 0
    for pm in itertools.permutations('9876543210'):
        if has_divisibility_property(pm):
            total += int(''.join(pm))
    print(total)
