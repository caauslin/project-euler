import math


def get_combinations(n, r):
    return math.factorial(n) // (math.factorial(r) * math.factorial(n - r))


if __name__ == '__main__':
    count = 0
    for n in range(1, 101):
        r_limit = n // 2 + 1  # reaches the max combinations at n // 2
        for r in range(1, r_limit):
            if get_combinations(n, r) > 1000000:
                count += (r_limit - r) * 2 - 1 if n % 2 == 0 else (r_limit - r) * 2
                break
    print(count)
