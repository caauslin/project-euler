import math
from euler import get_word_value, read_input


def is_triangle_num(n):
    """Checks if a number is a triangle number."""
    # the positive solution by quadratic formula
    x = (-1.0 + math.sqrt(1 - 4 * 1 * (-2 * n))) / 2.0
    return int(x) == x


if __name__ == '__main__':
    total = 0
    for word in read_input(strip='"', delim=','):
        if is_triangle_num(get_word_value(word)):
            total += 1
    print(total)
