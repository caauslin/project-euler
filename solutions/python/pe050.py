MAX_NUMBER = 999999

if __name__ == '__main__':
    prime_sieve = bytearray(MAX_NUMBER - 1)
    n = 2
    ulimit = MAX_NUMBER // 2
    while n < ulimit:
        if not prime_sieve[n - 2]:
            composite = n + n
            while composite <= MAX_NUMBER:
                prime_sieve[composite - 2] = 1
                composite += n
        n += 1

    max_prime_sum = 0
    max_terms = 0
    upper_limit = 0
    max_index = 0

    for upper_limit in range(MAX_NUMBER - 2, -1, -1):
        if prime_sieve[upper_limit]:
            continue
        upper_limit += 1
        break

    for fst in range(0, upper_limit):
        prime_sum = 0
        terms = 0
        for max_index in range(fst, upper_limit):
            if prime_sieve[max_index]:
                continue
            prime_sum += max_index + 2
            terms += 1
            if prime_sum > MAX_NUMBER:
                break
        if max_terms >= terms - 1:
            break
        for lst in range(max_index, fst, -1):
            if prime_sieve[max_index]:
                continue
            terms -= 1
            prime_sum -= lst + 2
            if max_terms >= terms:
                break
            elif prime_sieve[prime_sum - 2]:
                pass
            else:
                max_terms = terms
                max_prime_sum = prime_sum

    print(max_prime_sum)
