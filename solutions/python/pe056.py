if __name__ == '__main__':
    max_digital_sum = 0
    for a in range(1, 100):
        for b in range(1, 100):
            ab = a**b
            digital_sum = 0
            for d in str(ab):
                digital_sum += int(d)
            if digital_sum > max_digital_sum:
                max_digital_sum = digital_sum
    print(max_digital_sum)
