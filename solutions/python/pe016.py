def sum_digits_pow_2(p):
    """Sums all the digits in a power of 2."""
    if p < 0:
        return 0
    elif p == 0:
        return 1

    digits = [2]
    for _ in range(1, p):
        carry = False
        for i in range(0, len(digits)):
            r = digits[i] * 2
            if carry:
                r += 1
                carry = False
            if r >= 10:
                r -= 10
                carry = True
            digits[i] = r
        if carry: digits.append(1)

    return sum(digits)


if __name__ == '__main__':
    print(sum_digits_pow_2(1000))
