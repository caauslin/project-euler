def count_letters_in_num(n):
    """Finds the number of letters in a written number.

    Digits:
        one(3), two(3), three(5), four(4), five(4), six(3), seven(5), eight(5),
        nine(4)

    Tens:
        ten(3), eleven(6), twelve(6), thirteen(8), fourteen(8), fifteen(7),
        sixteen(7), seventeen(9), eighteen(8), nineteen(8)

        twenty(6), thirty(6), forty(5), fifty(5), sixty(5), seventy(7),
        eighty(6), ninety(6)

    Args:
        n (int): A number less than or equals to 1000.

    Returns:
        int: The number of letters.
    """
    total = 0

    if n == 1000:
        return 11
    else:
        if n >= 100:
            hundreds_lc = (10,10,12,11,11,10,12,12,11)
            hundred = int(str(n)[0])
            total += hundreds_lc[hundred - 1]
            n -= hundred * 100
            if n > 0:
                total += 3
        if n >= 20:
            tens2_lc = (6,6,5,5,5,7,6,6)
            ten2 = int(str(n)[0])
            total += tens2_lc[ten2 - 2]
            n -= ten2 * 10
        elif n >= 10:
            tens1_lc = (3,6,6,8,8,7,7,9,8,8)
            total += tens1_lc[n - 10]
            n -= n
        if n > 0:
            digits_lc = (3,3,5,4,4,3,5,5,4)
            total += digits_lc[n - 1]
    return total


if __name__ == '__main__':
    total = 0
    for i in range(1, 1001):
        total += count_letters_in_num(i)
    print(total)
