MAX_NUMBER = 999999


# FIXME use prime sieve util?
if __name__ == '__main__':
    prime_sieve = bytearray(MAX_NUMBER - 1)
    n = 2
    ulimit = MAX_NUMBER // 2
    while n < ulimit:
        if not prime_sieve[n - 2]:
            composite = n + n
            while composite <= MAX_NUMBER:
                prime_sieve[composite - 2] = 1
                composite += n
        n += 1

    count = 0
    for i in range(0, MAX_NUMBER - 1):
        if prime_sieve[i]:  # is composite
            continue
        n = i + 2

        n_str = str(n)
        if '0' in n_str:
            continue

        n_len = len(n_str)
        first_digit_factor = 10 ** (n_len - 1)
        is_circular_prime = True
        rotations = [n]
        for _ in range(0, n_len - 1):
            last_digit = n % 10
            n = first_digit_factor * last_digit + n // 10
            if n in rotations:
                continue
            if is_circular_prime and prime_sieve[n - 2]:
                is_circular_prime = False
            prime_sieve[n - 2] = 1
            rotations.append(n)

        if is_circular_prime:
            count += len(rotations)

    print(count)
