
import math


def is_positive_int(n):
    return n > 0 and math.ceil(n) == n


def resolve_quadratic_equation(a, b, c):
    """Returns the answers (can be the same) of a quadratic equation."""
    tmp = math.sqrt(b * b - 4 * a * c)
    return ((-b - tmp) / 2 / a, (-b + tmp) / 2 / a)


if __name__ == '__main__':
    tn = 285
    while True:
        tn += 1
        triangle = tn * (tn + 1) / 2

        pn = resolve_quadratic_equation(3, -1, -2 * triangle)[1]
        if not is_positive_int(pn):
            continue
        hn = resolve_quadratic_equation(2, -1, -triangle)[1]
        if not is_positive_int(hn):
            continue

        print(int(triangle))
        break
