from euler import read_input_lines


if __name__ == '__main__':
    rules = [[d for d in line] for line in read_input_lines()]
    passcode = rules.pop(0)
    while len(rules) > 0:
        for ri, rule in enumerate(rules):
            if len(rule) > 1 and passcode[0] == rule[1]:
                passcode.insert(0, rule.pop(0))
            if len(rule) > 1 and passcode[-1] == rule[-2]:
                passcode.append(rule.pop(-1))
            if len(rule) == 3:
                for i in range(len(passcode) - 1):
                    if (passcode[i], passcode[i + 1]) != (rule[0], rule[-1]):
                        continue
                    passcode.insert(i + 1, rule[1])
                    rule.clear()
                    break
            matches = [False] * len(rule)
            for pi in range(0, len(passcode)):
                try:
                    mi = matches.index(False)
                except ValueError:
                    break
                if rule[mi] == passcode[pi]:
                    matches[mi] = True
            if all(matches):
                rule.clear()
        rules = [r for r in rules if len(r) > 1]
    print(''.join(passcode))
