from euler import is_prime


def get_nth_prime(n):
    """Finds the n th prime number."""
    if n < 1:
        return 0
    elif n == 1:
        return 2
    # start from 3 (the 2nd prime number) to get rid of the only even prime
    index, num = 2, 3
    while index < n:
        num += 2
        if is_prime(num):
            index += 1
    return num


if __name__ == '__main__':
    print(get_nth_prime(10001))
