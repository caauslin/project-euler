def calc_pentagon(n):
    return n * (3 * n - 1) // 2


def calc_difference(j, k):
    """Returns the difference of P(k) and P(j), assuming j < k."""
    return (k - j) * (3 * (k + j) - 1) // 2


class PentagonDifferenceNode(object):

    def __init__(self, offset):
        self.j = 1
        self.offset = offset
        self.difference = calc_difference(self.j, self.j + offset)
        self.next_node = None

    def forward(self):
        self.j += 1
        self.difference = calc_difference(self.j, self.j + self.offset)


def sorted_diff():
    first_node = PentagonDifferenceNode(1)
    last_node = first_node
    node_count = 1

    while True:
        if first_node.offset >= node_count:
            node_count += 1
            # this will always be the largest difference
            last_node.next_node = PentagonDifferenceNode(node_count)
            last_node = last_node.next_node
        yield first_node

        first_node.forward()
        if first_node.difference <= first_node.next_node.difference:
            continue
        tmp_node = first_node
        first_node = first_node.next_node

        parent_node = first_node
        while not parent_node.next_node is None and \
                tmp_node.difference > parent_node.next_node.difference:
            parent_node = parent_node.next_node

        tmp_node.next_node = parent_node.next_node
        parent_node.next_node = tmp_node
        if tmp_node.next_node is None:
            last_node = tmp_node


def is_pentagonal(x):
    lower_limit, upper_limit = 1, 1
    while True:
        pn = calc_pentagon(upper_limit)
        if pn < x:
            upper_limit *= 10
        elif pn == x:
            return True
        else:
            lower_limit = upper_limit // 10
            break
    while True:
        n = (upper_limit + lower_limit) // 2
        pn = calc_pentagon(n)
        if pn > x:
            upper_limit = n
        elif pn < x:
            lower_limit = n
        else:
            return True
        if upper_limit - lower_limit <= 1:
            return False


# FIXME takes ~30m on MBA Early 2014
if __name__ == '__main__':
    for d in sorted_diff():
        if not is_pentagonal(d.difference):
            continue
        if is_pentagonal(calc_pentagon(d.j) + calc_pentagon(d.j + d.offset)):
            print(d.difference)
            break
