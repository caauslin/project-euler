def next_collatz_term(n):
    """Finds the next term in Collatz sequence."""
    if n % 2 == 0:
        return n // 2
    else:
        return n * 3 + 1


def get_collatz_length(n, cache={}):
    """Finds the length of a Collatz sequence with specific starting number.

    Args:
        n (int): The starting number

    Returns:
        (int, int): A tuple contains the length of the collatz sequence and the
            steps requires to calculate it.
    """
    chain_len = 1  # the starting number counts as 1 length
    step = 0  # the steps token to calculate the chain length
    while n != 1:
        if n in cache:
            chain_len += cache[n] - 1
            break
        n = next_collatz_term(n)
        chain_len += 1
        step += 1
    return (chain_len, step)


if __name__ == '__main__':
    max_chain_len, sn, cache, cache_hit_count = 0, 0, {}, 0
    for n in range(1, 1000000):
        (chain_len, step) = get_collatz_length(n, cache)
        # cache the result if it takes more than certain steps to calculate
        # memory usage ~ 8 MB
        if step >= 10:
            cache[n] = chain_len
        if max_chain_len < chain_len:
            max_chain_len = chain_len
            sn = n
    print(sn)
