import math
import sys


def is_prime(n):
    """Checks primality of specified integer."""
    if n == 2:
        return True
    elif n <= 1 or n % 2 == 0:
        return False

    for i in range(3, int(math.sqrt(n)) + 1, 2):
        if n % i == 0:
            return False
    return True


def is_permutation(lhs, rhs):
    """Checks if two strings are permutations of each other."""
    if len(lhs) != len(rhs):
        return False
    return sorted(lhs) == sorted(rhs)


def is_pandigital(n, zeroless=True):
    """Checks if a number is pandigital.

    Args:
        n (int|str): The number.
        zeroless (bool): The number includes zero or not.

    Returns:
        bool: True if the number is pandigital, False otherwise.
    """
    n_str = str(n)
    n_len = len(n_str)
    d_min = 1 if zeroless else 0
    d_max = n_len + d_min - 1

    if d_max > 10:
        return False
    for d in range(d_min, d_max):
        if not str(d) in n_str:
            return False
    return True


# copied from Python source code for convenience
def gcd(a, b):
    """Calculates the greatest common divisor of a and b."""
    while b:
        a, b = b, a % b
    return a


def _get_char_value(c):
    """Get the alphabetical value of a character.

    Args:
        c (str): The character.

    Returns:
        int: The alphabetical value of the character.
    """
    ascii_val = ord(c)
    if ascii_val >= 65 and ascii_val <= 90:
        return ascii_val - 64
    elif ascii_val >= 97 and ascii_val <= 122:
        return ascii_val - 96
    else:
        return 0


def get_word_value(word):
    """Get the alphabetical value of a string.

    Args:
        word (str): The word string.

    Returns:
        int: The alphabetical value of the word.
    """
    value = 0
    for i in range(0, len(word)):
        value += _get_char_value(word[i])
    return value


def read_input(strip=None, delim=(',',), value_type=str, argv_pos=1):
    """Generator. Yield data from input file one by one."""
    if isinstance(delim, str):
        delim = (delim,)
    with open(sys.argv[argv_pos], 'r') as f:
        value, eof = '', False
        while not eof:
            char = f.read(1)
            eof = not char
            if char in delim or eof:
                if eof and len(value) == 0:
                    break
                value = value.strip(strip) if strip else value
                if value_type == str:
                    yield value
                else:
                    yield value_type(value)
                value = ''
                if eof:
                    break
            else:
                value += char


def read_input_lines(argv_pos=1):
    """Generator. Yield data from input file line by line."""
    with open(sys.argv[argv_pos], 'r') as f:
        for line in f:
            yield line.strip()


class PrimeSieve(object):
    """A prime sieve is used for checking primality, using Sieve of Eratosthenes algorithm.

    NOTE:
        PrimeSieve is preferred over is_prime when you need to check primality of a lot of numbers,
        e.g. tens of thousands.

    BENCHMARK:
             TimeIt      | is_prime |   PrimeSieve
        -----------------|----------|----------------
        0 ~ 100  (10000) |   0.5569 |  1.0167 (183%)
        0 ~ 10000 (1000) |   7.7354 |  8.2039 (106%)
        0 ~ 1000000 (20) |  47.7569 | 15.2634 ( 32%)
    """

    # set a soft capacity at 64MB-size, prevent PrimeSieve grows without a limit
    SIEVE_SIZE_LIMIT = 1024 * 1024 * 64

    def __init__(self, sieve_size=1024):
        # make sure sieve size is even and no less than 4
        self._size = 4 if sieve_size <= 4 else sieve_size + \
            1 if sieve_size % 2 == 1 else sieve_size
        self._sieve = bytearray(b'\1\0' * (self._size // 2))
        self._build_sieve()

    def _build_sieve(self, start=0):
        # init setup
        if start == 0:
            self._sieve[1] = 1
            self._sieve[2] = 0
        # https://en.wikipedia.org/wiki/Sieve_of_Eratosthenes
        for n in range(3, int(math.sqrt(self._size)) + 1, 2):
            if self._sieve[n] == 1:  # is composite number
                continue
            composite = n + n
            while composite < self._size:
                if composite >= start:
                    self._sieve[composite] = 1
                composite += n

    def next(self, n, step=1):
        """Get the next prime starting from a certain number."""
        for _ in range(step):
            if n < 2:
                n = 2
            elif n == 2:
                n = 3
            else:
                n += 2
                while not self.is_prime(n):
                    n += 2
        return n

    def is_prime(self, n):
        """Check primality of given int."""
        if n >= self._size:
            while n >= self._size:
                self._size += self._size
                if self._size <= self.SIEVE_SIZE_LIMIT:
                    continue
                if n < self.SIEVE_SIZE_LIMIT:
                    self._size = self.SIEVE_SIZE_LIMIT
                else:
                    raise MemoryError(
                        f'PrimeSieve exceeds its soft capacity of {self.SIEVE_SIZE_LIMIT}')
            old_size = len(self._sieve)
            self._sieve = bytearray(
                self._sieve + b'\1\0' * ((self._size - old_size) // 2))
            self._build_sieve(old_size)
        return self._sieve[n] == 0

    def sieve(self, item):
        """Sieve a prime number, make it returns False in later prime check."""
        if self[item]:
            self._sieve[item] = 2

    def __getitem__(self, item):
        """Get the nth prime. Avoid to use it in loop, since it re-indexes from the beginning for every call."""
        if item < 0:
            raise IndexError('negative index is out of range')
        return self.next(1, item + 1)
