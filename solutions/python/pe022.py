from euler import read_input


def get_char_value(c):
    """Returns the alphabetical value of a character."""
    ascii_code = ord(c)
    if 65 <= ascii_code <= 90:
        return ascii_code - 64
    elif 97 <= ascii_code <= 122:
        return ascii_code - 96
    else:
        return 0


def get_word_value(word):
    value = 0
    for i in range(0, len(word)):
        value += get_char_value(word[i])
    return value


def total_name_scores():
    total_score, names = 0, []
    for word in read_input(strip='"'):
        names.append(word)
    names.sort()
    for i, name in enumerate(names):
        total_score += get_word_value(name) * (i + 1)
    return total_score


if __name__ == '__main__':
    print(total_name_scores())
