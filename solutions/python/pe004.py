def get_largest_palindrome_by_3d_times_3d():
    """Gets the largest palindrome equals to the product of two 3-digits int."""
    for digit in range(9, 0, -1):
        for tens_digit in range(9, -1, -1):
            for hundreds_digit in range(9, -1, -1):
                n = digit * 100001 + tens_digit * 10010 + hundreds_digit * 1100
                for d in range(100, 1000):
                    if n % d == 0 and len(str(n // d)) == 3:
                        return n


if __name__ == '__main__':
    print(get_largest_palindrome_by_3d_times_3d())
