import math
from euler import is_pandigital


def is_1_to_9_pandigital(n):
    """Checks if a number is 1 to 9 pandigital."""
    if len(str(n)) != 9:
        return False
    else:
        return is_pandigital(n)


if __name__ == '__main__':
    n = 2
    max_pandigit = 0
    while n < 10:
        num = 1
        while True:
            tmp = ''
            for i in range(1, n + 1):
                tmp = tmp + str(i * num)
            length = len(tmp)
            if length > 9:
                break
            tmp_int = int(tmp)
            if is_1_to_9_pandigital(tmp_int) and tmp_int > max_pandigit:
                max_pandigit = tmp_int
            num += 1
        n += 1
    print(max_pandigit)
