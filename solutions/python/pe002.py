if __name__ == '__main__':
    fib1, fib2, total = 1, 2, 0
    while fib2 <= 4000000:
        if fib2 % 2 == 0:
            total += fib2
        fib1_copy, fib1 = fib1, fib2
        fib2 = fib1_copy + fib2
    print(total)
