from euler import gcd


def simplify_faction(numerator, denominator):
    while True:
        divisor = gcd(numerator, denominator)
        if divisor == 1:
            return numerator, denominator
        numerator //= divisor
        denominator //= divisor


def iterate(numerator, denominator):
    # 1 + 1/(1 + n / d)
    return 2 * denominator + numerator, denominator + numerator


if __name__ == '__main__':
    count, n, d = 0, 1, 1
    for _ in range(0, 1000):
        n, d = iterate(n, d)
        n, d = simplify_faction(n, d)
        if len(str(n)) > len(str(d)):
            count += 1
    print(count)
