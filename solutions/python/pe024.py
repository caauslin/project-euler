import math


def get_nth_lexicographic_permutation(digits, n):
    """Finds the n th lexicographic permutation of given digits.

    Args:
        digits (list<int>): a list of digits (no duplicate).

    Returns:
        string: The n th lexicographic permutation (as integer type might omit
            the leading 0).
    """
    digits.sort()
    digits_len, steps = len(digits), n - 1
    while steps > 0:
        switch_index, step_count = -1, 0
        for i in range(1, digits_len + 1):
            if math.factorial(i) > steps:
                step_count = math.factorial(i - 1)
                switch_index = digits_len - i
                break
        assert switch_index != -1
        switched = False
        switch_digit = digits[switch_index]
        for i in range(switch_index + 1, digits_len):
            if digits[i] > switch_digit:
                digits[switch_index] = digits[i]
                digits[i] = switch_digit
                switched = True
                steps -= step_count
                break
        assert switched
    return ''.join([str(d) for d in digits])


if __name__ == '__main__':
    print(get_nth_lexicographic_permutation([0,1,2,3,4,5,6,7,8,9], 1000000))
