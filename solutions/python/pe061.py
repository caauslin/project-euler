import itertools


def triangle(n):
    return n * (n + 1) // 2


def square(n):
    return n * n


def pentagonal(n):
    return n * (3 * n - 1) // 2


def hexagonal(n):
    return n * (2 * n - 1)


def heptagonal(n):
    return n * (5 * n - 3) // 2


def octagonal(n):
    return n * (3 * n - 2)


class PolygonalCircle4DNode(object):

    def __init__(self, formulae, last_node=None):
        if not last_node:
            self._min = 1000
            self._max = 10000
        elif 1000 < last_node.value < 10000:
            self._min = last_node.value % 100 * 100
            self._max = self._min + 100
        else:
            raise ValueError()
        self._formulae = formulae
        self._n, self._pn = 1, self._formulae(1)
        while self._pn < self._min or self._pn % 100 < 10:
            self._move_next()

    @property
    def n(self):
        return self._n

    @property
    def value(self):
        if self._pn >= self._max:
            return None
        return self._pn

    def _move_next(self):
        self._n += 1
        self._pn = self._formulae(self._n)

    def next(self):
        self._move_next()
        while self._pn % 100 < 10:
            self._move_next()


class PolygonalCircle4D(object):

    def __init__(self, *formula):
        self._formula = formula

    def __iter__(self):
        nodes, circle, ri = [None] * len(self._formula), [0] * len(self._formula), 0
        while True:
            if nodes[ri] is None:
                nodes[ri] = PolygonalCircle4DNode(self._formula[ri], nodes[ri - 1])
            else:
                nodes[ri].next()
            circle[ri] = nodes[ri].value
            if circle[ri] is None:  # exceeds max
                if ri == 0:
                    break
                for _ in range(ri, len(circle)):
                    nodes[ri] = None
                ri -= 1
            elif ri == len(circle) - 1:
                if circle[ri] % 100 == circle[0] // 100:
                    yield circle
            else:
                ri += 1


def main():
    for formulae in itertools.permutations([triangle, square, pentagonal, hexagonal, heptagonal, octagonal]):
        for pr in PolygonalCircle4D(*formulae):
            return sum(pr)


if __name__ == '__main__':
    print(main())
