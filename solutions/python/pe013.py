from euler import read_input_lines

# FIXME brutally solved
if __name__ == '__main__':
    sum_n = 0
    for line in read_input_lines():
        line = line.rstrip()
        sum_n += int(line)
    print(str(sum_n)[0:10])
