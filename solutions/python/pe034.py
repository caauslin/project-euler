factorial_table = [1]
for n in range(1, 10):
    factorial_table.append(factorial_table[n - 1] * n)


def sum_digits_factorial(n):
    result = 0
    while n:
        digit = n % 10
        result += factorial_table[digit]
        n //= 10
    return result


# FIXME need optimise (takes ~20s on MBA Early 2014)
if __name__ == '__main__':
    ulimit = 9
    while ulimit <= sum_digits_factorial(ulimit):
        ulimit = ulimit * 10 + 9
    result = 0
    for n in range(10, ulimit):
        if sum_digits_factorial(n) == n:
            result += n
    print(result)
