def get_nth_digit(n):
    """Finds the n th digit of the fractional part of the irrational decimal
    fraction given in the problem.
    """
    m = 0
    while n > 0:
        m += 1
        n -= len(str(m))
    m_str = str(m)
    return int(m_str[len(m_str) - 1 + n])


if __name__ == '__main__':
    total = 1
    for n in (1, 10, 100, 1000, 10000, 100000, 1000000):
        total *= get_nth_digit(n)
    print(total)
