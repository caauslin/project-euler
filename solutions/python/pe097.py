def trim_num(n: int):
    return int(str(n)[-10:])


if __name__ == '__main__':
    TRIM_THRESHOLD = 2**127
    n = 2
    for _ in range(7830457 - 1):
        n *= 2
        if n >= TRIM_THRESHOLD:
            n = trim_num(n)
    n = trim_num(n)
    n = n * 28433 + 1
    print(trim_num(n))
