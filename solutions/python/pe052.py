from euler import is_permutation


def check_permutation(n):
    n_str = str(n)
    return is_permutation(n_str, str(n * 2)) and \
        is_permutation(n_str, str(n * 3)) and \
        is_permutation(n_str, str(n * 4)) and \
        is_permutation(n_str, str(n * 5)) and \
        is_permutation(n_str, str(n * 6))


if __name__ == '__main__':
    min_n = 1
    max_n = 10
    while True:
        for n in range(min_n, max_n // 6 + 1):
            if check_permutation(n):
                print(n)
                exit(0)
        min_n *= 10
        max_n *= 10
