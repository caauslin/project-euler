def sum_squares(max_n):
    """Sums the squares."""
    sum_n = 0
    for n in range(1, max_n + 1):
        sum_n += n * n
    return sum_n


def square_sum(max_n):
    """Square of the sum."""
    sum_n = (max_n + 1) * max_n // 2
    return sum_n * sum_n


if __name__ == '__main__':
    print(square_sum(100) - sum_squares(100))
