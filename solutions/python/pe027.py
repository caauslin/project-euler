from euler import is_prime


if __name__ == '__main__':
    max_n, product = 0, 0
    for a in range(-999, 1000):
        for b in range(-999, 1000):
            n = 0
            while is_prime(n * n + a * n + b):
                n += 1
            if n > max_n:
                max_n, product = n, a * b
    print(product)
