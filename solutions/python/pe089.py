from euler import read_input_lines


ROMAN_DENOMINATIONS = {
    'M': 1000,
    'D': 500,
    'C': 100,
    'L': 50,
    'X': 10,
    'V': 5,
    'I': 1
}

ROMAN_CONVS = [
    { 'threshold': 1000, 'roman': 'M' },
    # { 'threshold': 999, 'roman': 'IM' },
    # { 'threshold': 990, 'roman': 'XM' },
    { 'threshold': 900, 'roman': 'CM' },
    { 'threshold': 500, 'roman': 'D' },
    # { 'threshold': 499, 'roman': 'ID' },
    # { 'threshold': 490, 'roman': 'XD' },
    { 'threshold': 400, 'roman': 'CD' },
    { 'threshold': 100, 'roman': 'C' },
    # { 'threshold': 99, 'roman': 'IC' },
    { 'threshold': 90, 'roman': 'XC' },
    { 'threshold': 50, 'roman': 'L' },
    # { 'threshold': 49, 'roman': 'IL' },
    { 'threshold': 40, 'roman': 'XL' },
    { 'threshold': 10, 'roman': 'X' },
    { 'threshold': 9, 'roman': 'IX' },
    { 'threshold': 5, 'roman': 'V' },
    { 'threshold': 4, 'roman': 'IV' },
    { 'threshold': 1, 'roman': 'I' },
]


def to_number(roman_num):
    """Convert a roman numeral to number."""
    if len(roman_num) == 1:
        return ROMAN_DENOMINATIONS[roman_num]
    roman_digests = [ROMAN_DENOMINATIONS[d] for d in roman_num]
    for ri, rd in enumerate(roman_digests):
        if ri + 1 < len(roman_digests) and roman_digests[ri + 1] > rd:
            roman_digests[ri] = -rd
    return sum(roman_digests)


def to_roman(num):
    """Convert a number to roman number in minimal form."""
    roman_num = ''
    for conv in ROMAN_CONVS:
        while num >= conv['threshold']:
            num -= conv['threshold']
            roman_num += conv['roman']
    return roman_num


if __name__ == '__main__':
    saved = 0
    for rn in read_input_lines():
        saved += len(rn) - len(to_roman(to_number(rn)))
    print(saved)
