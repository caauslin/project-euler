import itertools
from euler import is_prime


def get_largest_pandigital_prime():
    """Finds the largest prime zeroless pandigital number."""
    ln = 9
    while ln > 1:
        n_str = '987654321'[-ln:]
        for seq in itertools.permutations(n_str):
            n = int(''.join(seq))
            if is_prime(n):
                return n
        ln -= 1
    return 'Not Found'


if __name__ == '__main__':
    print(get_largest_pandigital_prime())
