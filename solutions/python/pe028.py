def sum_diagonals_of_spiral_square(size):
    """Sums the four diagonals of numeric spiral square of given size."""
    largest_num = size * size
    return largest_num * 4 - (size - 1) * 6


if __name__ == '__main__':
    print(sum_diagonals_of_spiral_square(1001))
