from euler import is_prime


def list_primes():
    """Generator. Yields a sequence of primes in ascending order, the results
    will be cached.
    """
    yield 2
    yield 3

    n = 3
    try:
        for prime in list_primes.primes:
            n = prime
            yield prime
    except AttributeError:
        list_primes.primes = []

    while True:
        n += 2
        if is_prime(n):
            list_primes.primes.append(n)
            yield n


def count_prime_factors(n):
    """Returns the numbers of prime factor(s) of given number."""
    if is_prime(n):
        return 1

    count = 0
    for prime in list_primes():
        factor = False
        while n % prime == 0:
            factor = True
            n = n / prime
        if factor:
            count += 1
        if n == 1:
            break

    return count

# TODO takes ~30s
if __name__ == '__main__':
    n, consecutive_count = 646, 0
    while True:
        n += 1
        if count_prime_factors(n) == 4:
            consecutive_count += 1
            if consecutive_count == 4:
                print(n - 3)
                break
        else:
            consecutive_count = 0
