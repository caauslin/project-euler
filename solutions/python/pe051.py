import itertools
from euler import PrimeSieve


def replace_digits(num):
    digits = [int(d) for d in str(num)]
    # for eight prime value family the replacing part of the min number must be
    # less than 3, otherwise there won't be enough numbers
    for digit in range(0, 3):
        digit_indexes = [i for i in range(len(digits)) if digits[i] == digit]
        for digit_count in range(1, len(digit_indexes) + 1):
            for rd_indexes in itertools.combinations(digit_indexes, digit_count):
                digits_copy = digits.copy()
                digits_comb = []
                for td in range(digit, 10):
                    for rd_index in rd_indexes:
                        digits_copy[rd_index] = td
                    digits_comb.append(int(''.join(map(str, digits_copy))))
                yield digits_comb


def main():
    prime_sieve = PrimeSieve()
    n = 56993
    while True:
        n += 1
        if not prime_sieve.is_prime(n):
            continue
        for combinations in replace_digits(n):
            if sum(1 for comb in combinations if prime_sieve.is_prime(comb)) >= 8:
                return n


if __name__ == '__main__':
    print(main())
