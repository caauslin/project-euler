def is_palindromic(n):
    """Check if a number is palindromic."""
    n_str = str(n)
    n_len = len(n_str)
    for d in range(0, n_len // 2):
        if n_str[d] != n_str[n_len - 1 - d]:
            return False
    return True


def sum_palindromic_bin_dec(n_max):
    """Finds the sum of all numbers that are palindromic in both binary and
    decimal forms.

    Args:
        n_max (int): The maximum number (exclude).

    Returns:
        int: The sum.
    """
    total = 0
    for n in range(1, n_max):
        if is_palindromic(n) and is_palindromic('{0:b}'.format(n)):
            total += n
    return total


if __name__ == '__main__':
    print(sum_palindromic_bin_dec(1000000))
