from euler import is_prime


def count_divisors(n):
    """Finds the number of divisors of given natural number.
    @see http://primes.utm.edu/glossary/xpage/Tau.html
    """
    if n <= 1:
        return 1
    elif is_prime(n):
        return 2

    total, divisor = 1, 2
    while True:
        if is_prime(divisor):
            power = 0
            while n % divisor == 0:
                n = n // divisor
                power += 1
            if power > 0:
                total *= power + 1
                if n == 1:
                    return total
                elif is_prime(n):
                    return total * 2
        divisor += 1 if divisor == 2 else 2


if __name__ == '__main__':
    trig, n = 0, 1
    while True:
        trig += n
        count = count_divisors(trig)
        if count > 500:
            print(trig)
            break
        n += 1
