from euler import read_input_lines


def max_route_sum():
    routes = []  # possible max routes
    line_num = 0
    for line in read_input_lines():
        line_num += 1
        number_strings = line.split(' ')
        routes_copy = tuple(routes)
        for i, num_str in enumerate(number_strings):
            num = int(num_str)
            if line_num == 1:
                routes.append(num)
            elif i == 0:
                routes[i] += num
            elif i == line_num - 1:
                routes.append(num + routes_copy[-1])
            else:
                routes[i] = num + max(routes_copy[i], routes_copy[i - 1])
    return max(routes)


if __name__ == '__main__':
    print(max_route_sum())
