def operator_plus(m, n):
    f = m + n
    return int(str(f)[-10:])


def operator_multiply(m, n):
    f = m * n
    return int(str(f)[-10:])


def operator_power(m, n):
    f = 1
    while n > 0:
        f = operator_multiply(f, m)
        n -= 1
    return f


# TODO
if __name__ == '__main__':
    result = 0
    for n in range(1, 1001):
        result = operator_plus(result, operator_power(n, n))
    print(result)
