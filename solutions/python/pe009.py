import math


def get_product_of_pythagorean_triplets(sum_n):
    """Finds the products of pythagorean triplets who have the specific sum."""
    # the range of c is (sum / 3 + 1, sum_n - 2 - 1 + 1)
    for c in range(sum_n // 3 + 1, sum_n - 2):
        for b in range(2, min(c, sum_n - c - 1)):
            a = sum_n - c - b
            if a < b:
                if a * a + b * b == c * c:
                    return a * b * c


if __name__ == '__main__':
    print(get_product_of_pythagorean_triplets(1000))
