# FIXME could be optimized
def get_lowest_terms_fraction(n, d):
    """Get the lowest common fraction of given numerator and denominator.

    Args:
        n (int): The numerator.
        d (int): The denominator.

    Returns:
        (int, int): An two elements array in (numerator, denominator) form.
    """
    for divisor in range(2, min(n, d) // 2 + 1):
        while n % divisor == 0 and d % divisor == 0:
            n = n // divisor
            d = d // divisor

    return (n, d)


if __name__ == '__main__':
    product_nu = 1
    product_de = 1

    for nu in range(11, 99):
        if nu % 10 == 0:
            continue
        for de in range(nu + 1, 100):
            if de % 10 == 0:
                continue

            for c in str(nu):
                if c in str(de):
                    nu_new = int(str(nu).replace(c, '', 1))
                    de_new = int(str(de).replace(c, '', 1))
                    if nu_new * de == nu * de_new:
                        product_nu *= nu_new
                        product_de *= de_new

    print(get_lowest_terms_fraction(product_nu, product_de)[1])
