from euler import is_prime, is_permutation


def get_arithmetic_sequence(sorted_list):
    """Returns 3-terms arithmetic sequence from a sorted list if there is."""
    if len(sorted_list) < 3:
        return None
    # fst is the index of the first element in arithmetic sequence
    for fst in range(0, len(sorted_list) - 2):
        # lst is the index of the last element in arithmetic sequence
        for lst in range(len(sorted_list) - 1, fst + 1, -1):
            mid_term = (sorted_list[fst] + sorted_list[lst]) // 2
            if mid_term in sorted_list:
                return sorted_list[fst], mid_term, sorted_list[lst]
    return None


if __name__ == '__main__':
    primes = []
    for n in range(1001, 10000):
        if is_prime(n):
            primes.append(n)
    while len(primes) > 0:
        seq = list()
        seq.append(primes.pop(0))
        for i in range(len(primes) - 1, -1, -1):
            if is_permutation(str(seq[0]), str(primes[i])):
                # keep list sorted
                seq.insert(1, primes.pop(i))
        a_seq = get_arithmetic_sequence(seq)
        if a_seq is not None and a_seq != (1487, 4817, 8147):
            print(''.join([str(t) for t in a_seq]))
