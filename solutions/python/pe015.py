def count_grid_routes(a, b, caches={}):
    """Finds the total routes between two opposite corners of a grid.

    Args:
        a (int): The size of one side of the grid.
        b (int): The size of another side the grid.

    Returns:
        int: The total routes.
    """
    if a < 0 or b < 0:
        return 0

    x, y = max(a, b), min(a, b)
    if y == 0:
        return 1
    elif y == 1:
        return x + 1
    elif y == 2:
        return (x + 1) * (x + 2) // 2
    elif (x, y) in caches:
        return caches[(x, y)]

    total = 0
    for n in range(0, x + 1):
        total += count_grid_routes(n, y - 1, caches)
    # cache the result for better performance
    caches[(x, y)] = total
    return total


if __name__ == '__main__':
    print(count_grid_routes(20, 20))
