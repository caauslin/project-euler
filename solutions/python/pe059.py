import math
import euler


ASCII_A_LOWER = 97
ASCII_Z_LOWER = 122
ASCII_PRINTABLE_MIN = 32
ASCII_PRINTABLE_MAX = 126
# https://en.wikipedia.org/wiki/Letter_frequency
ENGLISH_LETTER_FREQUENCY_TABLE = {
    'a': 8.167,
    'b': 1.492,
    'c': 2.782,
    'd': 4.253,
    'e': 12.702,
    'f': 2.228,
    'g': 2.015,
    'h': 6.094,
    'i': 6.966,
    'j': 0.153,
    'k': 0.772,
    'l': 4.025,
    'm': 2.406,
    'n': 6.749,
    'o': 7.507,
    'p': 1.929,
    'q': 0.095,
    'r': 5.987,
    's': 6.327,
    't': 9.056,
    'u': 2.758,
    'v': 0.978,
    'w': 2.360,
    'x': 0.150,
    'y': 1.974,
    'z': 0.074,
}


def decrypt(cipher, key):
    text = [0 for _ in range(len(cipher))]
    for i in range(len(cipher)):
        text[i] = cipher[i] ^ key[i % len(key)]
    return text


def combinations(*iterables):
    indexes = [0 for _ in iterables]
    while True:
        comb = [0 for _ in iterables]
        for i in range(len(indexes)):
            comb[i] = iterables[i][indexes[i]]
        yield comb
        for i in range(len(indexes)):
            indexes[i] += 1
            if indexes[i] >= len(iterables[i]):
                indexes[i] = 0
            else:
                break
        if all(i == 0 for i in indexes):
            break


def calculate_paragraph_score(text):
    """Calculate a score (max 1, can be negative) indicates how likely an ascii code array is an english paragraph."""
    letter_frequencies = {}
    for lc in [chr(c).lower() for c in text]:
        if lc not in letter_frequencies:
            letter_frequencies[lc] = 1 / len(text)
        else:
            letter_frequencies[lc] += 1 / len(text)
    score = 26
    for char, frequency in ENGLISH_LETTER_FREQUENCY_TABLE.items():
        diff = math.fabs(letter_frequencies.get(char, 0) * 100 - frequency)
        score -= diff / frequency
    return score / 26


def main():
    keys = tuple(list(range(ASCII_A_LOWER, ASCII_Z_LOWER + 1)) for _ in range(3))
    pos = 0  # position index
    cipher = []  # encrypted text in form of ascii code array
    # filter impossible combinations that result to non-printable ascii characters
    for ascii_code in euler.read_input(value_type=int):
        key_idx = pos % 3
        if len(keys[key_idx]) > 1:
            bad_attempts = []
            for attempt in keys[key_idx]:
                ch = attempt ^ ascii_code
                if ch < ASCII_PRINTABLE_MIN or ch > ASCII_PRINTABLE_MAX:
                    bad_attempts.append(attempt)
            for bad_attempt in bad_attempts:
                keys[key_idx].remove(bad_attempt)
        cipher.append(ascii_code)
        pos += 1
    candidate = (0, [])  # (score, decrypted ascii array)
    for key in combinations(*keys):
        decrypted = decrypt(cipher, key)
        score = calculate_paragraph_score(decrypted)
        if score > candidate[0]:
            candidate = (score, decrypted)
    # print(''.join(chr(c) for c in candidate[1]))
    return sum(candidate[1])


if __name__ == '__main__':
    print(main())
