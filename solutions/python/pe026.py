def get_recurring_cycle_length(de):
    """Finds the recurring cycle length of a unit fraction.

    Args:
        de (int): The denominator.

    Returns:
        int: The recurring cycle length.
    """
    dividends, dividend = [], 1
    while True:
        while dividend < de:
            dividend *= 10
        if dividend in dividends:
            return len(dividends) - dividends.index(dividend)
        dividends.append(dividend)
        dividend = dividend % de
        if dividend == 0:
            return 0

if __name__ == '__main__':
    longest_rc_len = 0
    for de in range(2, 1000):
        rc_len = get_recurring_cycle_length(de)
        if rc_len > longest_rc_len:
            longest_rc_len = rc_len
    print(longest_rc_len)
