def main():
    result = 0
    for n in range(1, 10):  # n is the last digit of the number
        n_pow = 1
        while True:
            val = pow(n, n_pow)
            val_len = len(str(val))
            # power increased by 1 can only increase the length (digits) of the number by 1 at
            # most, thus we know this is the end of the loop, as keep increasing the power will
            # always generate a number with more digits than the power
            if val_len < n_pow:
                break
            elif val_len == n_pow:
                # print(f'{n_pow}-digits: {val} is {n_pow}th power of {n}')
                result += 1
            n_pow += 1
    return result


if __name__ == '__main__':
    print(main())
