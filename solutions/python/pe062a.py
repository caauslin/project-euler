def is_permutation(n1, n2):
    return sorted(str(n1)) == sorted(str(n2))


class CubicPermutationsLookup(object):

    def __init__(self, n_bgn, n_end, count):
        """Search cubic permutations by looping through combinations of given range of numbers.

        The combinations that will be checked is equal to:
            itertools.combinations(range(n_bgn, n_end + 1), count)

        However, it does not check every combination, e.g. for combination [n0, n1, n2, n3, n4], if
        n1^3 is not a permutation of n0^3, then combination in form of [n0, n1, _, _, _] are all
        not cubic permutations, the next combination to be checked will be [n0, n1 + 1, ...].
        """
        self._combination = [n_bgn + i for i in range(count)]
        self._max_num = n_end
        # numbers in the combination before this index are cubic permutations, so next time we only
        # need to check numbers after this index
        self._last_index = 0
        self._last_cube = n_bgn ** 3

    def search(self):
        max_num_0 = self._max_num - len(self._combination) + 1
        while True:
            if self._combination[0] > max_num_0:
                return None
            if self.search_current():
                return self._combination

    def search_current(self):
        """Check current combination, return True if it is cubic permutations."""
        for ci in range(self._last_index + 1, len(self._combination)):
            cube = self._combination[ci] ** 3
            if is_permutation(self._last_cube, cube):
                self._last_index = ci
                self._last_cube = cube
            else:
                self.next(ci)
                return False
        return True

    def next(self, index):
        """Calculate the next combination."""
        self._combination[index] += 1
        for i in range(index + 1, len(self._combination)):
            self._combination[i] = self._combination[index] + i - index
        overflow = next((i for i in range(len(self._combination))
                         if self._combination[i] > self._max_num), -1)
        if overflow == 0:
            # end of search
            return False
        elif overflow > 0:
            return self.next(overflow - 1)
        else:
            # update the last cubic permutations index
            if index == 0:
                self._last_index = 0
                self._last_cube = self._combination[0] ** 3
            elif self._last_index >= index:
                self._last_index = index - 1
                self._last_cube = self._combination[self._last_index] ** 3
            return True


def get_cubic_permutations():
    # smallest 3 cubic permutations starts at 41063625 (8 digits)
    digits_len, n_bgn, n_end = 8, 1, -1
    # permutations must have same digits count
    # so we check all combinations of numbers whose cubes have same digits count, one by one,
    # starting from 8
    while True:
        c_bgn = 10 ** (digits_len - 1)
        c_end = 10 ** digits_len - 1
        if n_end == -1:
            while True:
                if n_bgn ** 3 >= c_bgn:
                    break
                n_bgn += 1
        else:
            n_bgn = n_end + 1
        n_end = n_bgn + 1
        while True:
            if n_end ** 3 > c_end:
                n_end -= 1
                break
            n_end += 1
        # print(f'checking: {n_bgn} to {n_end}')
        lookup = CubicPermutationsLookup(n_bgn, n_end, 5)
        result = lookup.search()
        if result:
            return result
        digits_len += 1


# initial slow solution takes 30+ seconds
if __name__ == '__main__':
    print(get_cubic_permutations()[0] ** 3)
