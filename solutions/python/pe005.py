def least_common_multiple(max_divisor):
    product, prime_factors = 1, []
    for n in range(2, max_divisor + 1):
        for i in prime_factors:
            if n % i == 0:
                n //= i
        prime_factors.append(n)
        product *= n
    return product


if __name__ == '__main__':
    print(least_common_multiple(20))
