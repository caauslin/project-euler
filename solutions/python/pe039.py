import math


def count_right_angle_triangle_solutions(p):
    """Finds the number of solutions for a right angle triangle with given
    perimeter.
    """
    total = 0
    # assume a <= b < c
    for c in range(p // 3, p // 2):
        for b in range((p - c) // 2 + 1, c):
            a = p - c - b
            if a > b:
                continue
            elif a * a + b * b == c * c:
                total += 1
    return total


if __name__ == '__main__':
    n, total_max = 0, 0
    for p in range(6, 1001):
        total = count_right_angle_triangle_solutions(p)
        if total > total_max:
            total_max = total
            n = p
    print(n)
