from multiprocessing import Pool
from euler import is_prime


def sum_primes_range(rng):
    """Finds the sum of all prime numbers within a given range."""
    sum_n = 0
    for n in range(rng[0], rng[1]):
        if is_prime(n):
            sum_n += n
    return sum_n


if __name__ == '__main__':
    # use multiple threads for better performance
    pool = Pool(processes=4)
    results = pool.map(sum_primes_range, (
        (2,  500000),
        (500000, 1000000),
        (1000000, 1500000),
        (1500000, 2000000)
    ))
    print(sum(results))
