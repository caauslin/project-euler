import math


def get_largest_prime_factor(n, m=2):
    """Finds the larget of prime factor of given number.

    Args:
        n (int): The number.

    Returns:
        int: The largest prime factor.
    """
    for i in range(m, int(math.sqrt(n)) + 1):
        if n % i == 0:
            return get_largest_prime_factor(n // i, i)
    return n


if __name__ == '__main__':
    print(get_largest_prime_factor(600851475143))
