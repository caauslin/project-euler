def is_leap_year(year):
    """Checks if a given year is leap year."""
    if year % 400 == 0:
        return True
    elif year % 100 == 0:
        return False
    elif year % 4 == 0:
        return True
    else:
        return False


class Date:
    def __init__(self, day, month, year):
        self.day = day
        self.month = month
        self.year = year
        self.is_leap = is_leap_year(self.year)

    def get_days_in_mouth(self):
        """Gets the number of days in current month."""
        if self.month == 4 or self.month == 6 or self.month == 9 or self.month == 11:
            return 30
        elif self.month == 2:
            if self.is_leap:
                return 29
            else:
                return 28
        else:
            return 31

    def forward(self, days_num):
        """Move date forward for specific number of days."""
        self.day += days_num
        while self.day > self.get_days_in_mouth():
            self.day -= self.get_days_in_mouth()
            self.month += 1
            if self.month > 12:
                self.month -= 12
                self.year += 1
                self.is_leap = is_leap_year(self.year)


if __name__ == '__main__':
    date = Date(1, 1, 1900)
    # 1900/1/1 is Monday, the next Sunday will be six days later
    date.forward(6)

    total = 0
    while date.year <= 2000:
        if date.year >= 1901 and date.day == 1:
            total += 1
        date.forward(7)
    print(total)
