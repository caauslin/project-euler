#include <stdio.h>

const int FACTORIALS[] = {1, 1, 2, 6, 24, 120, 720, 5040, 40320, 362880};

long sum_digits_factorial(long n)
{
    long result = 0;
    while (n) {
        long digit = n % 10;
        result += FACTORIALS[digit];
        n /= 10;
    }
    return result;
}

int main()
{
    long ulimit = 9;
    long result = 0;

    // calculate upper limit
    while (ulimit <= sum_digits_factorial(ulimit)) {
        ulimit = ulimit * 10 + 9;
    }

    for (long n = 10; n < ulimit; ++n) {
        if (sum_digits_factorial(n) == n) {
            result += n;
        }
    }

    printf("%li\n", result);
    return 0;
}
