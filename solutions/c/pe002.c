#include <stdio.h>

#define MAX_NUMBER 4000000

int main()
{
    long sum = 0;
    long term[2] = {1, 2};

    // the fib terms follows the pattern: (odd even odd) (odd even odd) ...
    while (term[1] <= MAX_NUMBER) {
        sum += term[1];

        long tmp = term[0] + term[1];
        term[0] = tmp + term[1];
        term[1] = term[0] + tmp;
    }

    printf("%li\n", sum);
    return 0;
}
