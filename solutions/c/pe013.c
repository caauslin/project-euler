#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define ASCII_ZERO 48
#define ASCII_NINE 57

#define DIGIT_LEN 54

int main(int argc, char *argv[])
{
    char line[52];
    char digits[DIGIT_LEN] = "";

    FILE *fp = fopen(argv[1], "r");
    if (fp == NULL) {
        perror("Unable to open input file");
        exit(1);
    }

    while (fgets(line, sizeof(line), fp) != NULL) {
        int digit_index = 0;
        for (int i = strlen(line) - 1; i >= 0; --i) {
            if (line[i] < ASCII_ZERO || line[i] > ASCII_NINE) {
                continue;
            }

            char digit = line[i] - ASCII_ZERO + digits[digit_index];
            if (digit >= 10) {
                int next = digit_index;
                do {
                    digits[next] = digit - 10;
                    digit = digits[++next] + 1;
                } while (digit >= 10);
                digits[next] = digit;
            } else {
                digits[digit_index] = digit;
            }
            digit_index++;
        }
    }

    fclose(fp);

    int digit_count = 0;
    for (int i = DIGIT_LEN - 1; i >= 0; --i) {
        if (!digit_count && digits[i] == 0) {
            continue;
        } else if (digit_count == 10) {
            break;
        }
        printf("%c", digits[i] + ASCII_ZERO);
        digit_count++;
    }
    printf("\n");
    return 0;
}
