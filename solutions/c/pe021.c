#include <stdio.h>

#define MAX_NUMBER 10000

// returns the sum of the proper divisors
long sum_divisors(long n)
{
    if (n <= 1) {
        return 0;
    }
    long sum = 1;
    for (long d = 2, limit = n / 2 + 1; d < limit; d++) {
        if (n % d == 0) {
            sum += d;
        }
    }
    return sum;
}

int is_amicable(long n)
{
    long d = sum_divisors(n);
    return d != n && sum_divisors(d) == n;
}

int main() {
    long sum = 0;
    for (long n = 1; n < MAX_NUMBER; n++) {
        if (is_amicable(n)) {
            sum += n;
        }
    }

    printf("%li\n", sum);
    return 0;
}
