#include <stdio.h>

int is_leap_year(int year)
{
    return year % 400 == 0 || (year % 100 != 0 && year % 4 == 0);
}

int get_max_days(int month, int year)
{
    switch (month) {
        case 9:
        case 4:
        case 6:
        case 11:
            return 30;
            break;
        case 2:
            return is_leap_year(year) ? 29 : 28;
            break;
        default:
            return 31;
    }
}

// return 1 if date 1 exceed date 2, 0 otherwise
int is_date_exceed(int year1, int month1, int day1,
                   int year2, int month2, int day2)
{
    if (year1 < year2) {
        return 0;
    } else if (year1 > year2) {
        return 1;
    } else if (month1 < month2) {
        return 0;
    } else if (month1 > month2) {
        return 1;
    } else if (day1 <= day2) {
        return 0;
    } else {
        return 1;
    }
}

int main()
{
    int sundays = 0;
    int day = 1, month = 1, year = 1900, weekday = 1;
    int maxDay = get_max_days(month, year);
    while (!is_date_exceed(year, month, day, 2000, 12, 31)) {
        if (year >= 1901 && day == 1 && weekday == 7) {
            sundays++;
        }
        day++;
        weekday++;
        if (day > maxDay) {
            day = 1;
            month++;
            if (month > 12) {
                month = 1;
                year++;
            }
        }
        if (weekday > 7) {
            weekday = 1;
        }
    }

    printf("%i\n", sundays);
    return 0;
}
