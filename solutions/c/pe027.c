#include <stdio.h>
#include "euler.h"

int main()
{
    int max_consecutive_primes = 0;
    int product_of_ab = 0;

    for (int a = -999; a <= 999; a++)
    {
        for (int b = -999; b <= 999; b++)
        {
            int n = 0;
            int consecutive_primes = 0;

            while (1)
            {
                if (is_prime(n * n + a * n + b))
                {
                    n++;
                    consecutive_primes++;
                } else
                {
                    break;
                }
            }

            if (consecutive_primes > max_consecutive_primes)
            {
                max_consecutive_primes = consecutive_primes;
                product_of_ab = a * b;
            }
        }
    }

    printf("%d\n", product_of_ab);
    return 0;
}
