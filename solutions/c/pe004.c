#include <stdio.h>
#include <stdlib.h>

int main()
{
    for (long units = 9; units > 0; --units) {
        for (long tens = 9; tens >= 0; --tens) {
            for (long hundreds = 9; hundreds >= 0; --hundreds) {
                // this is the palindrome, loop in descending
                long n = units * 100001 + tens * 10010 + hundreds * 1100;
                for (long divisor = 101; divisor < 1000; ++divisor) {
                    if (n % divisor == 0) {
                        long quotient = n / divisor;
                        if (quotient >= 100 && quotient <= 999) {
                            printf("%li\n", n);
                            return EXIT_SUCCESS;
                        }
                    }
                }
            }
        }
    }

    return EXIT_FAILURE;
}
