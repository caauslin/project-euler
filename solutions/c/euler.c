
int is_prime(long x)
{
    if (x <= 1) return 0;
    else if (x == 2) return 1;
    else if (x % 2 == 0) return 0;

    long divisor = 3;
    while (divisor * divisor <= x) {
        if (x % divisor == 0) {
            return 0;
        }
        divisor += 2;
    }

    return 1;
}
