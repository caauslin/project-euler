#include <stdio.h>
#include "euler.h"

int main()
{
    int prime_count = 0;
    long num = 1;

    while (prime_count < 10001) {
        num++;
        if (is_prime(num)) {
            prime_count++;
        }
    }

    printf("%li\n", num);
    return 0;
}
