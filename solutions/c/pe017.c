#include <stdio.h>

// letter counts for 100, 200, ... 900
static int HUNDREDS_LETTER_COUNT[] = {10,10,12,11,11,10,12,12,11};
// letter counts for 20, 30, ... 90
static int TENS_LETTER_COUNT[] = {6,6,5,5,5,7,6,6};
// letter counts from 1 to 19
static int DIGITS_LETTER_COUNT[] = {3,3,5,4,4,3,5,5,4,3,6,6,8,8,7,7,9,8,8};

int count_number_letters(int n)
{
    if (n > 1000 || n < 1) {
        fprintf(stderr, "number (%d) is out of range (1-1000)\n", n);
        return 0;
    }

    if (n == 1000) {
        return 11;
    } else {
        int result = 0;
        if (n >= 100) {
            int hundreds = n / 100;
            result += HUNDREDS_LETTER_COUNT[hundreds - 1];
            n -= hundreds * 100;
            if (n > 0) {
                result += 3; // "and"
            }
        }
        if (n >= 20) {
            int tens = n / 10;
            result += TENS_LETTER_COUNT[tens - 2];
            n -= tens * 10;
        }
        if (n > 0) {
            result += DIGITS_LETTER_COUNT[n - 1];
        }

        return result;
    }
}

int main()
{
    int count = 0;
    for (int i = 1; i <= 1000; ++i) {
        count += count_number_letters(i);
    }

    printf("%d\n", count);
    return 0;
}
