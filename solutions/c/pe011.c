#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define GRID_SIZE 20

int main(int argc, char *argv[])
{
    long grids[GRID_SIZE][GRID_SIZE];
    int x = 0;
    int y = 0;

    char line[GRID_SIZE * 3 + 1];
    char *token;

    FILE *fp = fopen(argv[1], "r");
    if (fp == NULL) {
        perror("Unable to open input file");
        exit(1);
    }

    while (fgets(line, sizeof(line), fp) != NULL) {
        token = strtok(line, " ");
        while (token != NULL) {
            grids[y][x] = strtol(token, NULL, 10);
            token = strtok(NULL, " ");
            if (++x == GRID_SIZE) {
                x = 0;
                y++;
            }
        }
    }

    fclose(fp);

    long max_product = 0;
    long product;

    for (x = 0; x < GRID_SIZE; ++x) {
        for (y = 0; y < GRID_SIZE - 4; ++y) {
            // horizontal
            product = grids[x][y] * grids[x][y + 1] * grids[x][y + 2] *
                      grids[x][y + 3];
            if (product > max_product) max_product = product;
            // vertical
            product = grids[y][x] * grids[y + 1][x] * grids[y + 2][x] *
                      grids[y + 3][x];
            if (product > max_product) max_product = product;
        }
    }

    for (x = 3; x < GRID_SIZE; ++x) {
        for (y = 0; y < GRID_SIZE - 4; ++y) {
            // diagonal (slash)
            product = grids[y][x] * grids[y + 1][x - 1] * grids[y + 2][x - 2] *
                      grids[y + 3][x - 3];
            if (product > max_product) max_product = product;
            // diagonal (backslash)
            product = grids[x][y] * grids[x - 1][y + 1] * grids[x - 2][y + 2] *
                      grids[x - 3][y + 3];
            if (product > max_product) max_product = product;
        }
    }

    printf("%li\n", max_product);
    return 0;
}
