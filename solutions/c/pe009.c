#include <stdio.h>

#define SUM 1000

int min(int x, int y)
{
    return x < y ? x : y;
}

int main()
{
    for (int c = SUM / 3 + 1, max_c = SUM - 2; c < max_c; ++c) {
        for (int b = 2, max_b = min(c, SUM - c - 1); b < max_b; ++b) {
            int a = SUM - b - c;
            if (a < b && a * a + b * b == c * c) {
                printf("%d\n", a * b * c);
                return 0;
            }
        }
    }

    return 1;
}
