#include <stdio.h>

#define MAX_NUM 1000000L

long next_collatz(long n)
{
    if (n % 2 == 0) {
        return n / 2;
    } else {
        return n * 3 + 1;
    }
}

long count_collatz_chain(long n)
{
    long count = 1;
    while (n != 1) {
        n = next_collatz(n);
        count++;
    }
    return count;
}

int main()
{
    long max_count = 0;
    long result = 0;

    for (long n = 1; n < MAX_NUM; ++n) {
        long count = count_collatz_chain(n);
        if (count > max_count) {
            max_count = count;
            result = n;
        }
    }

    printf("%li\n", result);
    return 0;
}
