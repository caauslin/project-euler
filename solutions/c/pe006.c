#include <stdio.h>

#define MAX_NUMBER 100

int main()
{
    long square_sum = 0;
    long sum_square = 0;

    for (long n = 1; n <= MAX_NUMBER; ++n) {
        square_sum += n * n;
        sum_square += n;
    }
    sum_square *= sum_square;

    printf("%li\n", sum_square - square_sum);
    return 0;
}
