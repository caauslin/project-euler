#include <stdio.h>

#define GRID_WIDTH 20
#define GRID_HEIGHT 20

static unsigned long RESULT_CACHE[GRID_WIDTH][GRID_HEIGHT];

/*
Solution:

Given a "x*y" grid, and f(x,y) is the number of routes.

So obviously,

f(x,y) = f(y,x)
f(x,0) = 1
f(x,1) = x + 1

Also,

f(x,y)
    = f(x,y-1) + f(x-1,y-1) + f(x-2,y-1) + ... + f(2,y-1) + f(1,y-1) + f(0,y-1)

And,

f(x,2)
    = f(x,1) + f(x-1,1) + f(x-2,1) + ... f(1,1) + f(0,1)
    = (x+1) + x + (x-1) + ... + 2 + 1
    = (x+1)(x+2)/2

Thus, f(x,y) can be calculated recursively.
*/

unsigned long count_lattice_routes(unsigned long x, unsigned long y)
{
    if (x < y) {
        return count_lattice_routes(y, x);
    }

    if (y == 0) {
        return 1;
    } else if (y == 1) {
        return x + 1;
    } else if (y == 2) {
        return (x + 1) * (x + 2) / 2;
    } else if (RESULT_CACHE[x][y] != 0) {
        return RESULT_CACHE[x][y];
    }

    unsigned long total = 0;
    for (unsigned long i = 0, limit = x + 1; i < limit; ++i) {
        total += count_lattice_routes(i, y - 1);
    }

    RESULT_CACHE[x][y] = total;
    return total;
}

int main()
{
    printf("%lu\n", count_lattice_routes(GRID_WIDTH, GRID_HEIGHT));
    return 0;
}
