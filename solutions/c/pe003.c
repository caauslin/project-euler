#include <stdio.h>

int main()
{
    unsigned long n = 600851475143;
    unsigned long factor = 3; // since n is odd, so we can ignore 2

    while (factor * factor <= n) {
        while (n % factor == 0 && n != factor) {
            n /= factor;
        }
        factor += 2;
    }

    printf("%lu\n", n);
}
