#include <stdio.h>
#include "euler.h"

int calc_divisors(long n)
{
    if (n <= 1) return 1;
    else if (is_prime(n)) return 2;

    long count = 1;
    long divisor = 2;
    while (1) {
        if (is_prime(divisor)) {
            long power = 0;
            while (n % divisor == 0) {
                n = n / divisor;
                power++;
            }

            if (power > 0) {
                count *= power + 1;
                if (n == 1) return count;
                else if (is_prime(n)) return count * 2;
            }
        }

        divisor += divisor == 2 ? 1 : 2;
    }
}

int main()
{
    long n = 1;
    long tri = 0;
    while (1) {
        tri += n++;
        if (calc_divisors(tri) > 500) {
            break;
        }
    }

    printf("%li\n", tri);
    return 0;
}
