#include <stdio.h>
#include <math.h>

int sum_divisors(const long n)
{
    long total = 0;
    for (long d = 1, m = n / 2; d <= m; d++)
    {
        total += (n % d == 0) ? d : 0;
    }
    return total;
}

int is_abundant(const long n)
{
    return sum_divisors(n) > n ? 1 : 0;
}

int is_sum_of_two_abundant(const long n)
{
    for (int x = 12, m = (long) ceil(n / 2.0); x <= m; x++)
    {
        int y = n - x;
        if (is_abundant(x) && is_abundant(y))
        {
            return 1;
        }
    }
    return 0;
}

// FIXME takes couple of minutes
int main()
{
    long total = 0;
    for (long n = 1; n <= 28123; n++)
    {
        if (!is_sum_of_two_abundant(n))
        {
            total += n;
        }
    }
    printf("%li\n", total);
    return 0;
}
