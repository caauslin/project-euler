#include <stdio.h>

#define MAX_NUMBER 1999999

// primality of natural numbers starting from 2
static char prime_sieve[MAX_NUMBER - 1] = "";

int main()
{
    long sum = 0;
    long n = 2;
    long ulimit = MAX_NUMBER / 2;

    // search and mark composite numbers
    while (n < ulimit) {
        if (!prime_sieve[n - 2]) {
            sum += n;
            long composite = n + n;
            while (composite <= MAX_NUMBER) {
                prime_sieve[composite - 2] = 1;
                composite += n;
            }
        }
        n++;
    }

    for (long i = ulimit; i < MAX_NUMBER - 1; ++i) {
        sum += prime_sieve[i] ? 0 : i + 2;
    }

    printf("%li\n", sum);
    return 0;
}
