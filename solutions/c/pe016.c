#include <stdio.h>
#include <string.h>

#define ANSCII_ZERO 48
#define DIGIT_LEN 500

void double_num(char *num)
{
    char add[DIGIT_LEN];
    memcpy(add, num, DIGIT_LEN);

    for (int i = 0; i < DIGIT_LEN; ++i) {
        char digit = num[i] + add[i];
        if (digit >= 10) {
            int next = i;
            do {
                num[next] = digit - 10;
                digit = num[++next] + 1;
            } while (digit >= 10);
            num[next] = digit;
        } else {
            num[i] = digit;
        }
    }
}

int main()
{
    char num[DIGIT_LEN] = "";
    num[0] = 2;

    for (int i = 0; i < 999; ++i) {
        double_num(num);
    }

    int sum = 0;
    for (int i = DIGIT_LEN - 1; i >= 0; --i) {
        if (num[i] != 0) {
            sum += (int) num[i];
        }
    }

    printf("%d\n", sum);
    return 0;
}
