#include <stdio.h>
#include <stdlib.h>
#include "euler.h"

int main()
{
    long n = 1;
    long side_length = 3;
    long prime_count = 0;
    long number_count = 1;

    while (1) {
        for (int i = 0; i < 4; ++i) {
            n += side_length - 1;
            number_count++;
            if (is_prime(n)) {
                prime_count += is_prime(n);
            } else if (prime_count * 10 < number_count) {
                printf("%li\n", side_length);
                return EXIT_SUCCESS;
            }
        }

        side_length += 2;
    }

    return EXIT_FAILURE;
}
