# Problem 67 - Maximum path sum II

By starting at the top of the triangle below and moving to adjacent numbers on the row below, the maximum total from top to bottom is 23.

&nbsp;&nbsp;&nbsp;<b>3</b><br>
&nbsp;&nbsp;<b>7</b> 4<br>
&nbsp;2 <b>4</b> 6<br>
8 5 <b>9</b> 3

That is, 3 + 7 + 4 + 9 = 23.

Find the maximum total from top to bottom in triangle.txt (see 067.txt), a 15K text file containing a triangle with one-hundred rows.

NOTE: This is a much more difficult version of Problem 18. It is not possible to try every route to solve this problem, as there are 2^99 altogether! If you could check one trillion (10^12) routes every second it would take over twenty billion years to check them all. There is an efficient algorithm to solve it. ;o)
