# Project Euler Solutions

Repository for my solutions of [Project Euler](https://projecteuler.net/).

### Prerequisites:

* Bash
* Python 3

Requirements for each language:



### Usage

#### Run a specific solution file

```
./euler run {path/to/solution_file}
```

For example,

```
./euler run solutions/python/pe001.py
```

#### Run a solution for specific question in specific language

```
./euler run {language}{question_#}
```

For example,

```
./euler run py1
```

#### Clean up temporary files

```
./euler clean
```
